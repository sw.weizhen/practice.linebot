/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import (
	"practice.linebot/cmd"
)

func main() {
	cmd.Execute()
}
