package router

import (
	"fmt"
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/v7/linebot"
	"go.mongodb.org/mongo-driver/bson"
	opv "practice.linebot/modules"
)

func Ping(engine *gin.Engine) {
	engine.GET("/ping", func(c *gin.Context) {
		c.JSON(200, "pong")
	})
}

func ListMsg(engine *gin.Engine) {
	engine.GET("list/:user_id/msg", func(c *gin.Context) {
		userID := c.Param("user_id")

		msgList := &[]opv.UserMsg{}
		filter := bson.M{"UserID": userID}
		err := opv.MG.FindMany(
			opv.CST_MONGO_DATABASE,
			opv.CST_MONGO_COLLECTION_USERMSG,
			100,
			filter,
			msgList,
		)

		if err != nil {
			c.JSON(500, gin.H{
				"success": false,
				"errCode": 10001,
				"errMsg":  err.Error(),
				"data":    nil,
			})

			return
		}

		data := make([]string, 0)
		for _, msg := range *msgList {
			data = append(data, msg.Message)
		}

		c.JSON(200, gin.H{
			"success": true,
			"errCode": -1,
			"errMsg":  nil,
			"data":    data,
		})

	})
}

func Sender(engine *gin.Engine) {
	engine.POST("/sender", func(c *gin.Context) {

		userID := c.PostForm("user_id")
		msg := c.PostForm("msg")

		_, err := opv.LB.PushMessage(userID, linebot.NewTextMessage(msg)).Do()
		if err != nil {
			c.Writer.WriteHeader(400)
			c.Writer.WriteString(fmt.Sprintf("push message failed, err: %s", err.Error()))

			c.JSON(500, gin.H{
				"success": false,
				"errCode": 10000,
				"errMsg":  err.Error(),
				"data":    nil,
			})

			return
		}

		c.JSON(200, gin.H{
			"success": true,
			"errCode": -1,
			"errMsg":  nil,
			"data":    nil,
		})
	})
}

func Receiver(engine *gin.Engine) {
	engine.POST("/receiver", func(c *gin.Context) {

		events, err := opv.LB.ParseRequest(c.Request)
		if err != nil {
			if err == linebot.ErrInvalidSignature {
				c.Writer.WriteHeader(400)

			} else {
				c.Writer.WriteHeader(500)

			}

			return
		}

		for _, event := range events {
			if event.Type == linebot.EventTypeMessage {
				switch message := event.Message.(type) {
				case *linebot.TextMessage:
					userID := event.Source.UserID
					info, err := opv.LB.GetProfile(userID).Do()
					if err != nil {
						log.Printf("parsing user profile err -> %v", err.Error())
						return
					}

					displayName := info.DisplayName
					msg := message.Text

					userInfo := bson.D{
						{Key: "$set", Value: bson.D{
							{Key: "UserID", Value: userID},
							{Key: "DisplayName", Value: displayName},
						}},
					}

					timestamp := time.Now().UnixNano()
					userMsg := bson.D{
						{Key: "UserID", Value: userID},
						{Key: "Message", Value: msg},
						{Key: "Timestamp", Value: timestamp},
					}

					filter := bson.M{"UserID": userID}
					_, _, _, err = opv.MG.UpdateOne(
						opv.CST_MONGO_DATABASE,
						opv.CST_MONGO_COLLECTION_USERINFO,
						filter,
						userInfo,
					)
					if err != nil {
						log.Printf("upsert user info %s err, cause: %s\n", userID, err)
						return
					}

					_, err = opv.MG.InsertOne(
						opv.CST_MONGO_DATABASE,
						opv.CST_MONGO_COLLECTION_USERMSG,
						userMsg,
					)
					if err != nil {
						log.Printf("insert user(%s) msg: %s err, cause: %s\n", userID, msg, err)
						return
					}

				default:
				}
			}
		}
	})
}
