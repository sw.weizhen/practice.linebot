package modules

import (
	"github.com/line/line-bot-sdk-go/v7/linebot"
	swmongo "practice.linebot/modules/mongodb"
)

var LB *linebot.Client
var MG *swmongo.MongoOperator

var Err error
