package modules

const (
	CST_MONGO_DATABASE            = "linebot_users"
	CST_MONGO_COLLECTION_USERINFO = "info"
	CST_MONGO_COLLECTION_USERMSG  = "msg"
)
