package modules

type UserInfo struct {
	Id          string `bson:"_id,omitempty" json:"ID"`
	UserID      string `bson:"UserId" json:"UserID"`
	DisplayName string `bson:"DisplayName" json:"DisplayName"`
}

type UserMsg struct {
	Id        string `bson:"_id,omitempty" json:"ID"`
	UserID    string `bson:"UserId" json:"UserID"`
	Timestamp int64  `bson:"Timestamp" json:"Timestamp"`
	Message   string `bson:"Message" json:"Message"`
}
