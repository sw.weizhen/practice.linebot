package swmongo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoOperator struct {
	monClient *mongo.Client
	hostName  string
	connPort  int
}

type DocResponse struct {
	Doc    bson.D
	Length int
	RtnIDs []string
}
