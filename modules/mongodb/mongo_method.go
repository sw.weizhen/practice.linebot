package swmongo

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo/options"
)

func (ref *MongoOperator) IsAlive() bool {

	if ref.monClient == nil {
		return false
	}

	ctx, ctxCancel := context.WithTimeout(context.TODO(), time.Second*5)
	defer ctxCancel()

	if err := ref.monClient.Ping(ctx, nil); err != nil {
		return false
	}

	return true
}

func (ref *MongoOperator) Disconnect() bool {
	if err := ref.monClient.Disconnect(context.TODO()); err != nil {
		return false
	}

	return true
}

func (ref *MongoOperator) InsertOne(databaseName string, collectionName string, docObject interface{}) (string, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	insertRes, err := collection.InsertOne(context.TODO(), docObject)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%v", insertRes.InsertedID), nil
}

func (ref *MongoOperator) UpdateOne(databaseName string, collectionName string, filter interface{}, update interface{}) (int, int, int, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	opts := options.Update().SetUpsert(true)
	updateRes, err := collection.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		return -1, -1, -1, err
	}

	return int(updateRes.ModifiedCount), int(updateRes.UpsertedCount), int(updateRes.MatchedCount), err
}

func (ref *MongoOperator) FindOne(databaseName string, collectionName string, filter interface{}, objPointer interface{}) error {
	if objPointer == nil {
		return fmt.Errorf("nil pointer")
	}

	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	singleResult := collection.FindOne(context.TODO(), filter)

	err := singleResult.Decode(objPointer)
	if err != nil {
		return err
	}

	return nil
}

func (ref *MongoOperator) FindMany(databaseName string, collectionName string, maxRows int, filter interface{}, objListPointer interface{}) error {
	if objListPointer == nil {
		return fmt.Errorf("nil pointer")
	}

	if maxRows <= 0 {
		return fmt.Errorf("maxRows must be greater than 0")
	}

	findOpts := options.Find()
	findOpts.SetLimit(int64(maxRows))

	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	cursor, err := collection.Find(context.TODO(), filter, findOpts)
	if err != nil {
		return err
	}

	defer cursor.Close(context.TODO())

	if errCur := cursor.All(context.TODO(), objListPointer); errCur != nil {
		return errCur
	}

	return nil
}
