package main

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	swmongo "practice.linebot/modules/mongodb"
)

var mon *swmongo.MongoOperator
var err error

var data bson.D

func init() {
	mon, err = swmongo.New("192.168.9.1", 27017)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("MongoDB is alive: %t\n", mon.IsAlive())

	data = bson.D{
		{Key: "Name", Value: "Veronica"},
		{Key: "Location", Value: "Nowhere"},
		{Key: "Series", Value: bson.A{1, 2, 3, 4, 5, 6, 7, 8, 9}},
	}
}

type UserData struct {
	Id       string `bson:"_id,omitempty" json:"id"`
	Name     string `bson:"Name" json:"Name"`
	Location string `bson:"Location" json:"Location"`
	Series   []int  `bson:"Series" json:"Series"`
}

func main() {
	fmt.Println("main::main()")

	// v, e := mon.InsertOne("test", "data", data)
	// fmt.Println(v, e)

	// userData := &UserData{}
	// filter := bson.M{"Name": "Gillain"}
	// err := mon.FindOne("test", "data", filter, userData)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// fmt.Printf("data: %v\n", userData)

	// upd := bson.D{
	// 	{Key: "Name", Value: "Veronica"},
	// 	{Key: "Location", Value: ""},
	// 	{Key: "Series", Value: bson.A{}},
	// }
	// bson.D{
	//     {"$set", bson.D{{"author", "Nic Raboy"}}},
	// },

	filter := bson.M{"Name": "XXXXX"}
	upd := bson.D{
		{Key: "$set", Value: bson.D{{Key: "Location", Value: "Nic Raboy"}}},
	}
	a, b, c, err := mon.UpdateOne("test", "data", filter, upd)

	if err != nil {
		fmt.Printf("err: %v\n", err)
	}

	fmt.Println(a, b, c)

}
