package swmongo

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func New(host string, port int) (*MongoOperator, error) {

	applyURI := fmt.Sprintf("mongodb://%s:%d", host, port)

	mgClient, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(applyURI))
	if err != nil {
		return nil, err
	}

	if mgClient == nil {
		return nil, fmt.Errorf("mongo nil pointer")
	}

	if err := mgClient.Ping(context.TODO(), nil); err != nil {
		return nil, fmt.Errorf("fail to ping, error: %v", err)
	}

	return &MongoOperator{
		monClient: mgClient,
		hostName:  host,
		connPort:  port,
	}, nil
}
