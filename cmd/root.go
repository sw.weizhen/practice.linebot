package cmd

import (
	"fmt"
	"log"

	bot "github.com/line/line-bot-sdk-go/v7/linebot"
	vpr "github.com/spf13/viper"
	opv "practice.linebot/modules"
	mon "practice.linebot/modules/mongodb"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "provide web api service",
	Short: "web api",
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Printf("[err]init failed: %v\n", err)
	}
}

func init() {

	vpr.SetConfigName("cfg")
	vpr.SetConfigType("yaml")
	vpr.AddConfigPath("./")

	opv.Err = vpr.ReadInConfig()
	if opv.Err != nil {
		log.Fatalf("read config failed, err: %v", opv.Err)
	}

	opv.MG, opv.Err = mon.New(vpr.GetString("mongodb.host"), vpr.GetInt("mongodb.port"))
	if opv.Err != nil {
		log.Fatalf("mongo init failed, err: %v", opv.Err)
	}

	opv.LB, opv.Err = bot.New(
		vpr.GetString("linebot.channel_secret"),
		vpr.GetString("linebot.channel_token"),
	)

	if opv.Err != nil {
		log.Fatalf("linebot init failed, err: %v", opv.Err)
	}

	rootCmd.AddCommand(apiCmd)
}
