package cmd

import (
	"fmt"
	"net/http"
	"time"

	r "practice.linebot/api"

	g "github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	vpr "github.com/spf13/viper"
)

var httpServer *http.Server

var apiCmd = &cobra.Command{
	Use:   "api",
	Short: "apiCmd",
	Long:  "apiCmd provide api interface service",
	Run: func(cmd *cobra.Command, args []string) {
		address := fmt.Sprintf("%s:%d", "0.0.0.0", vpr.GetInt("linebot.port"))

		ginEng := g.New()

		r.Ping(ginEng)
		r.Receiver(ginEng)
		r.Sender(ginEng)
		r.ListMsg(ginEng)

		httpServer = &http.Server{
			Addr:        address,
			Handler:     ginEng,
			IdleTimeout: time.Minute,
		}

		if err := httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			fmt.Println("init failed", err)
		}

	},
}
